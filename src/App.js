import React from 'react'
import { useState } from 'react'
import Header from './components/Header'
import Tasks from './components/Tasks'
import AddTask from './components/AddTask'

const App = () => {

    const[addTaskForm, showAddTaskForm] = useState(false);

    const [tasks, setTask] = useState([
        {
            id: 1,
            title: 'Have Lunch',
            time: 'Today 1.00PM',
            reminder: true
        },
        {
            id: 2,
            title: 'Work',
            time: 'Today 2.00PM',
            reminder: true
        },
        {
            id: 3,
            title: 'Change Buddi diaper',
            time: 'Today 10.00PM',
            reminder: true
        }
    ]);

    //Deleting a Task
    const onDeleteTask = (id) => {
        setTask(
            tasks.filter(task => (task.id !== id))
        )
    }

    //Toggling Reminder of a Task
    const onToggleReminder = (id) => {
        setTask(
            tasks.map(task => 
                task.id === id ? 
                    {
                        ...task, reminder: !task.reminder
                    } :
                    task
                )
        )
    }

    //Add a Task to the
    const onAddTask = (task) => {
        const id = Math.floor(Math.random() * 10000) + 1
        const newTask = {id, ...task}
        setTask([...tasks, newTask])
    }

    //Show Add Task Form
    const onShowAddTask = () => {
        showAddTaskForm(!addTaskForm)
    }
    
    return (
        <div className='container'>
           <Header title='Task Tracker' onShowAddTask={onShowAddTask} addTaskForm={addTaskForm}/>
           { addTaskForm && <AddTask onAddTask = {onAddTask}/>}
            { tasks.length !== 0 ? 
                <Tasks tasks = {tasks} onDeleteTask={onDeleteTask} onToggleReminder={onToggleReminder}/> 
                : <h3>Nothing to show</h3>
           } 
        </div>
    )
}

export default App