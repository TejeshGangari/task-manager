import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ color, text, addTaskForm, onClick }) => {

    return (

            <button style={{backgroundColor : color}} className='btn' onClick={onClick}>{text}</button>

    )
}

Button.defaultProps = { 
    color: 'red',
    text: 'Add Button Text'
}

Button.propTypes = {
    color: PropTypes.string,
    text: PropTypes.string
}

export default Button
