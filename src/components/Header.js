import React from 'react'
import Button from './Button'

const Header = ({title, onShowAddTask, addTaskForm}) => {

    return (
        <header className='header'>
            <h1>{title}</h1>
            <Button color = {addTaskForm ? 'red': 'green'}
                    text = {addTaskForm ? 'Close': 'Add'}
                    onClick={onShowAddTask}
            />
        </header>
    )
}

export default Header