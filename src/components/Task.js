import React from 'react'
import {FaTimes} from 'react-icons/fa'

const Task = ({task, onDeleteTask, onToggleReminder}) => {
    return (
        <div className={`task ${task.reminder ? `reminder` : ``}`} onDoubleClick={() => onToggleReminder(task.id)}>
            <h3 id={task.id}>
                {task.title} 
                <FaTimes style={{color: 'red', cursor: 'pointer'}} 
                onClick={() => onDeleteTask(task.id)}
                /> 
            </h3>
            <p>{task.time}</p>
        </div>
    )
}

export default Task
