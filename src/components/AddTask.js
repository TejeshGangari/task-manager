import React from 'react'
import { useState } from 'react'

const AddTask = ({onAddTask}) => {

    const [title, setText] = useState('')
    const [time, setDay] = useState('')
    const [reminder, setReminder] = useState(false)

    const onSubmit = (e) => {
        e.preventDefault();

        if(!title){
            alert("Please add Task Title")
            return;
        }

        onAddTask({title, time, reminder});

        setText('')
        setDay('')
        setReminder(false)

    }


    return (
        <form className='add-form' onSubmit={onSubmit}>
            <div className='form-control'>
                <label>Task</label>
                <input type='text' 
                placeholder='Task Title' 
                value={title} 
                onChange={ e => setText(e.target.value)}
                />
            </div>
            <div className='form-control'>
                <label>Day & Time</label>
                <input type='datetime-local' 
                placeholder='Day & Time'
                value={time} 
                onChange={ e => setDay(e.target.value)}   
                />
            </div>
            <div className='form-control form-control-check'>
                <label>Set Reminder</label>
                <input type='checkbox'
                checked={reminder}
                value={reminder} 
                onChange={ e => setReminder(e.currentTarget.checked)}    
                />
            </div>
            <div className='form-control'>
                <input type='submit' value='Save Task' className='btn btn-block'/>
            </div>
        </form>
    )
}
export default AddTask
