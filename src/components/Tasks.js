import Task from './Task'

const Tasks = ( { tasks, onDeleteTask, onToggleReminder } ) => {
    return (
        <>
         {tasks.map( (task) => (
             <Task task={task} onDeleteTask={onDeleteTask} onToggleReminder={onToggleReminder}/>
         ))}  
        </> 
    );
}

export default Tasks
